class CashMachine
  def initialize
    @balance = 100
    @stateMenu = '0'
    @bank_account = 'Balance.txt'
  end

  def menu
    puts "Выберите действие:\n(Q) Quit\n(D) Deposit\n(W) Withdraw\n(B) Balance\nYour choice: "
    @stateMenu = gets.chomp
  end

  def init
    menu
    while (@stateMenu != 'Q')
      case @stateMenu
      when 'D'
        deposit
        menu
      when 'W'
        withdraw
        menu
      when 'B'
        balance
        menu
      else
        puts 'Invalid menu point!'
      end
    end
    puts 'Done!'
  end

  def deposit
    puts 'Введите сумму, которую хотите вложить:'
    depositSum = gets.to_i
    if depositSum > 0
      @balance += depositSum
    else puts 'Неверная сумма!'
    end
    puts "Ваш баланс: #{@balance}"
  end

  def withdraw
    puts 'Введи сумму, которую хотите снять:'
    withdrawSum = gets.to_i
    if (withdrawSum > 0) && (withdrawSum <= @balance)
      @balance -= withdrawSum
    else puts 'Неверная сумма!'
    end
    puts "Ваш баланс: #{@balance}"
  end

  def balance
    puts "Ваш баланс: #{@balance}"
  end
end

bankAccount = CashMachine.new()
bankAccount.init