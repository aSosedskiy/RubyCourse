require 'rspec'
require_relative './main'

RSpec.describe "foo" do
  it "do ok for CS last symbols" do
    str = "i play cs"
    expect(foo(str)).to eq 2 ** str.length
  end
  it  "do ok for another last symbols" do
    str = "i play valorant"
    expect(foo(str)).to eq str.reverse
  end
end
