class AgeFile

  def main()
    puts "Чтобы продолжать работу введите y"
    printStudents = false
    studentsCount = 0
    appropriateStudentCount = 0

    result = File.new("Lab4/task2/task2result.txt", "w")
    while(true)
      puts "Введите ваш возраст"
      data = gets.chomp();

      age = Integer(data)
      File.foreach('Lab4/task2/task2.txt').with_index do |content, index|

        if (content.include?(age.to_s))
          result.puts(content)
          appropriateStudentCount += 1
        end
        studentsCount = index
      end

      puts "\n Подолжить? \n"
      data = gets.chomp()

      if(data != "y")
        result.close()
        break
      end
    end

    puts "Чтобы вывести всех студентов без побочного условия введите -1"
    if(gets.chomp() == "-1" || appropriateStudentCount == studentsCount)
      puts "Вывод найденных студентов"
      File.foreach('task1Result.txt').with_index do |content, index|
        puts content
      end
    end
  end

end

ageFile = AgeFile.new()
ageFile.main()

