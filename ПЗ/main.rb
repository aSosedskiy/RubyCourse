
def findEqualDigits(array)
  newArray = Array.new
  for element in array
    if(element % 2 == 0)
      newArray.push(element)
    end
  end
  return newArray;
end

def findNotEqualDigits(array)
  newArray = Array.new
  for element in array
    if(element % 2 != 0)
      newArray.push(element)
    end
  end
  return newArray;
end

def sumDigit(digit)

  arrayOfDigits = digit
  sum = 0
  while(digit >= 1)
    sum += digit % 10
    digit /= 10;
  end

  return sum
end

def printArray(array)
  for element in array
    print(element.to_s + ' ')
  end
end

array = [1, 2, 3, 5, 8, 13, 21]

(printArray array)
puts
puts 'Четные элементы'
printArray(findEqualDigits(array))
puts
puts 'Нечетные элементы'
printArray(findNotEqualDigits(array))
puts
puts 'Сумма элементов числа 2321'
puts sumDigit(2321)
