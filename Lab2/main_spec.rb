require 'rspec'
require './main.rb'

RSpec.describe "Main" do
  it "#greeting" do
    expect(greeting("Антон", 17)).to eq("Привет, Антон. Тебе меньше 18 лет, но начать учиться программировать никогда не рано.")
  end
  it "#foobar" do
    expect(foobar(20, 2)).to eq(2)
    expect(foobar(2, 18)).to eq(20)
  end
end