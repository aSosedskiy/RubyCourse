class TaskFile
  def initialize(filePath)
    @filePath = filePath
  end

  def index
    file = File.open(@filePath)
    puts file.readlines.map(&:chomp)

    file.close()
  end

  def find(id)
    file = File.open(@filePath)
    result = file.read().split("\n")

    file.close()

    puts result[id]
  end

  def where(pattern)
    result = Array.new()
    File.foreach(@filePath).with_index do |content, index|
      if (content.include?(pattern))
        result.push(index)
      end
    end
    puts result.to_s
    return result
  end

  def update(id, text)
    bufferFileName = 'buffer.txt'
    file = File.open(bufferFileName, 'w')
    File.foreach(@filePath).with_index do |content, index|
      file.puts(id == index ? text : content)
    end

    file.close()

    File.write(@filePath, File.read(bufferFileName))
    File.delete(bufferFileName) if File.exist?(bufferFileName)
  end

  def delete(id)
    bufferFileName = 'buffer.txt'

    file = File.open(bufferFileName, 'w')
    File.foreach(@filePath).with_index do |content, index|
      if (id != index)
        file.puts(content)
      end
    end

    file.close()

    File.write(@filePath, File.read(bufferFileName))
    File.delete(bufferFileName) if File.exist?(bufferFileName)
  end

end

taskFile = TaskFile.new('Lab4/task1/task1.txt')
taskFile.index()
taskFile.find(2)
taskFile.where('mir')
taskFile.update(3,'Vladimir Nikogosyan')
taskFile.delete(5)
