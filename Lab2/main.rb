def greeting(name, age)
  if age < 18
    "Привет, #{name}. Тебе меньше 18 лет, но начать учиться программировать никогда не рано."
  else
    "Привет,#{name}. Самое время заняться делом!"
  end
end

name = gets.chomp
age = gets.to_i

puts greeting(name, age)

def foobar(first, second)
  if first == 20 or second == 20
    second
  else
    first + second
  end
end

first = gets.to_i
second = gets.to_i

puts foobar(first, second)